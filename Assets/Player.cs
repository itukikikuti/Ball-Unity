﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] Rigidbody rigidbody;
    [SerializeField] float speed;

    void Update()
    {
        if (Input.GetKey(KeyCode.W))
        {
            rigidbody.AddForce(0f, 0f, speed);
        }

        if (Input.GetKey(KeyCode.S))
        {
            rigidbody.AddForce(0f, 0f, -speed);
        }

        if (Input.GetKey(KeyCode.A))
        {
            rigidbody.AddForce(-speed, 0f, 0f);
        }

        if (Input.GetKey(KeyCode.D))
        {
            rigidbody.AddForce(speed, 0f, 0f);
        }
    }
}
