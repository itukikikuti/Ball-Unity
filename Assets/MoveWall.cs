﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveWall : MonoBehaviour
{
    enum State
    {
        Start,
        End
    }

    [SerializeField] Rigidbody rigidbody;
    [SerializeField] float speed = 0.1f;
    [SerializeField] Vector3 start;
    [SerializeField] Vector3 end;

    State state = State.Start;

    void Start()
    {
        transform.position = start;
    }

    void Update()
    {
        switch (state)
        {
            case State.Start:

                rigidbody.MovePosition(Vector3.MoveTowards(transform.position, start, speed));

                if (transform.position == start)
                {
                    state = State.End;
                }

                break;
            case State.End:

                rigidbody.MovePosition(Vector3.MoveTowards(transform.position, end, speed));

                if (transform.position == end)
                {
                    state = State.Start;
                }

                break;
        }
    }
}
