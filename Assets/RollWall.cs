﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RollWall : MonoBehaviour
{
    [SerializeField] Rigidbody rigidbody;
    [SerializeField] float speed = 0.1f;

    float angle = 0f;

    void Update()
    {
        angle += speed;
        rigidbody.MoveRotation(
            Quaternion.Euler(0f, angle, 0f));
    }
}
