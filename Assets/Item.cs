﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    [SerializeField] AudioClip sound;

    void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "Player")
        {
            AudioSource.PlayClipAtPoint(sound, transform.position);
            Destroy(gameObject);
        }
    }
}
