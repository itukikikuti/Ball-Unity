﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [SerializeField] Text scoreText;
    [SerializeField] GameObject clearText;

    void Update()
    {
        int count = GameObject.FindGameObjectsWithTag("Item").Length;
        scoreText.text = count.ToString();

        if (count == 0)
        {
            clearText.SetActive(true);
        }
    }
}
