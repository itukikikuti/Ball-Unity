﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    [SerializeField] Transform target;

    void Update()
    {
        transform.position = target.position + new Vector3(0f, 8f, -1f);
    }
}
